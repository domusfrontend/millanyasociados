<?php
$uno='adm';
$dos='adm*1';
?>
<!DOCTYPE html>
<html lang="es">
<head>
<title>millan & asociados</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width; initial-scale=1.0">
<meta name="description" content="Nuestra experiencia de más de 15 años en el mercado nos permite asesorarlo en la compra, venta, 						arrendamiento o avalúo de sus inmuebles." />
<meta name="keywords" content="inmuebles, apartamentos, casas, locales, bodegas, oficinas, fincas, lotes, arrendamiento, arriendo, 					ventas, compra, avaluos, bogota, manizales" />
<meta name="author" content="dynamicweb" />
<link rel="shortcut icon" href="images/simbolo.png">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<script src="js/jquery-1.7.1.min.js"></script>
<script src="js/script.js"></script>
<script src="js/superfish.js"></script>
<script src="js/jquery.responsivemenu.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/FF-cash.js"></script>

    <!--ocultar mostrar-->
<script type="text/javascript" src="js/ocultar.js"></script>
<script type="text/javascript">
<? if($_REQUEST['gestion']){$gestion=$_REQUEST['gestion'];}else{$gestion=1;} ?>
$(function(){ ocultarinicio(<? echo $gestion; ?>) });
</script>

<style type="text/css">
.slideshow {margin:0 -40px }
.slideshow img { padding: 10px; border: 1px solid #ccc; background-color: #eee; }
</style>
<!-- include jQuery library -->
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>-->
<!-- include Cycle plugin -->
<script type="text/javascript" src="http://malsup.github.com/jquery.cycle.all.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.slideshow').cycle({
		fx: 'fade' // choose your transition type, ex: fade, scrollUp, shuffle, etc...
	});
});
</script>

<!--[if lt IE 8]>
   <div style=' clear: both; text-align:center; position: relative;'>
     <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
       <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
  </div>
<![endif]-->
<!--[if lt IE 9]>
	<script src="js/html5.js"></script>
	<link rel="stylesheet" href="css/ie.css">
<![endif]-->

 <!-- Begin popup-->
    <script src="Scripts/swfobject_modified.js" type="text/javascript"></script>
    <SCRIPT LANGUAGE="JavaScript">
	function popUp(URL) {
	day = new Date();
	id = day.getTime();

	eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=600,height=650,left = 340,top = 0');");
}
</script>
<!--// End popup -->
</head>
<body>
<!-- header -->
<header>
	<div class="container_24">
		<div class="grid_24">
			<div class="fleft"><a href="index.php"><img src="/images/logo.png" alt=""></a></div>
			<ul class="sf-menu">
				<li class="current"><a href="index.php">Inicio</a></li>
				<li><a href="nosotros.html">Nosotros</a></li>
                <li><a href="servicios.html">Servicios</a><!--<ul>
								<li><a href="#">Arrendamiento<span class="arrow"></span></a></li>
								<li><a href="#">Compra y Venta de Inmuebles</a></li>
								<li><a href="#">Avalúo de Inmuebles</a></li>
								<li><a href="#">Administración de Inmuebles</a></li>
                                <li><a href="#">Asesoría Jurídica</a></li>
                                <li><a href="#">Reparaciones Locativas</a></li>
                                <li><a href="#">Seguros</a></li>
							</ul>-->
						</li>
                <li><a href="inmuebles.php">Inmuebles</a></li>
                <li><a href="clientes.html">Clientes</a></li>
                <li><a href="contacto.html">Contacto</a></li>
                <li><a href="pqrs.html">Pqrs</a></li>
                <li><a href="pagospse.html">Pagos Pse</a></li>
			</ul>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</header>

<!-- content -->
<section>
	<div class="bg">
		<div class="container_24">
			<div class="wrapper">
<span style="font:14px/16px Arial, Helvetica, sans-serif;color:#7c7876; float:left; margin-top:8px;"><strong>Somos una organización prestadora de servicios inmobiliarios profesionales en las ciudades de Manizales y Bogotá.</strong></span>

            <div class="social">
            	<a target="_blank" href="https://www.instagram.com/millanyasociadosinmobiliaria/"><img src="images/instagram.png" height="30" width="30"></a>
            	<a target="_blank" href="https://www.facebook.com/millanyasociados/?ref=br_rs&pnref=lhc"><img src="images/facebook.png" height="30" width="30"></a>
            	<a target="_blank" href="https://twitter.com/MILLANASOCIADOS"><img src="images/twitter.png" height="30" width="30"></a>
            </div>

				<div class="grid_8 padtop3">

                <div class="buscador">





        <form method="get" name="action1" id="action1" action="inmuebles.php">
            <table width="300" height="150" border="1" cellpadding="0" cellspacing="3">
            <tr>
            	<td colspan="2">
            	  Arriendo <input id="gestion" name="gestion" type="radio" value="1" onChange="enableArriendo(1)" checked  >
            	  Venta <input id="gestion" name="gestion" type="radio" value="2" onChange="enableArriendo(2)" >
          	  </td>
                </tr>

  <tr>
  <td height="25">Ciudad

  </td>
  <td align="right">
 <select style="background:#D40E1E;font: 14px Arial, Helvetica, sans-serif;color:#FFFFFF; outline: medium none;width:150px;height:25px;float:left;" id="ciudad" name="ciudad">
          	<option  selected="selected" value="0">Ciudad [Todas]</option>
            <option  value="11001">Bogotá</option>
            <option  value="25126">Cajicá</option>
            <option  value="73148">Carmen de Apicalá</option>
            <option  value="13001">Cartagena</option>
            <option  value="25175">Chía</option>
            <option  value="25183">Chocontá</option>
            <option  value="25214">Cota</option>
            <option  value="25307">Girardot</option>
            <option  value="25326">Guatavita</option>
            <option  value="25377">La Calera</option>
            <option  value="5001">Medellín</option>
            <option  value="25473">Mosquera</option>
            <option  value="25736">Sesquilé</option>
            <option  value="25743">Silvania</option>
            <option  value="25754">Soacha</option>
            <option  value="25758">Sopó</option>
            <option  value="25799">Tenjo</option>

            <?php
			/*$verciudades='http://domus.la/comercial/ctrl/Class.ApiCtrl.php?accion=consultarCiudades&sbUsuario=PAULAM&sbClave=1984';
			$obj = json_decode(file_get_contents($verciudades),true);


 			 foreach ($obj['Ciudades'] as $var)
			 {
				echo "<option value=".$var['datos']['nuCodigo'].">".$var['datos']['sbNombre']."</option>";
			 }*/
				?>

  </select>

    </td>
  </tr>
  <tr>
    <td>Tipo Inmueble</td>
    <td align="right">
    <select style="background:#D40E1E;font: 14px Arial, Helvetica, sans-serif;color:#FFFFFF; outline: medium none;width:150px;height:25px;float:left;" id="tipo" name="tipo">
          	<option  selected="selected" value="0">Tipo Inmueble [Todos]</option>
    			<?php
		/*	$vergestiones='http://domus.la/comercial/ctrl/Class.ApiCtrl.php?accion=consultarGestiones&sbUsuario=PAULAM&sbClave=1984';
			$obj = json_decode(file_get_contents($vergestiones),true);


 			 foreach ($obj['Gestiones'] as $var)
			 {
				echo "<option value=".$var['datos']['nuId'].">".$var['datos']['sbNombre']."</option>";
			 }*/
				?>
                <?php
			$vertipos='http://domus.la/comercial/ctrl/Class.ApiCtrl.php?accion=consultarTiposInmueble&sbUsuario='.$uno.'&sbClave='.$dos;
			$obj = json_decode(file_get_contents($vertipos),true);


 			 foreach ($obj['TiposInmueble'] as $var)
			 {
				echo "<option value=".$var['datos']['nuId'].">".ucwords($var['datos']['sbNombre'])."</option>";
			 }
				?>

                </select>
    </td>
  </tr>
  <tr>
    <td>Zona</td>
    <td align="right">
    <select style="background:#D40E1E;font: 14px Arial, Helvetica, sans-serif;color:#FFFFFF; outline: medium none;width:150px;height:25px;float:left;" id="zona" name="zona">
                        <option  selected="selected" value="0">Zona [Todas]</option>
						<?php
			$vergestiones='http://domus.la/comercial/ctrl/Class.ApiCtrl.php?accion=consultarZonas&sbUsuario='.$uno.'&sbClave='.$dos;
			$obj = json_decode(file_get_contents($vergestiones),true);


 			 foreach ($obj['Zonas'] as $var)
			 {
				echo "<option value=".$var['datos']['nuId'].">".ucwords($var['datos']['sbNombre'])."</option>";
			 }
				?>
                        </select>

    </td>
  </tr>
  <tr>
    <td>Precio Máximo</td>
    <td align="right">
    <select style="background:#D40E1E;font: 14px Arial, Helvetica, sans-serif;color:#FFFFFF; outline: medium none;width:150px;height:25px;float:left;" id="presupuesto" name="presupuesto">

                          <option value="" selected="selected">Todos</option>
                          <option value="500000">$ 500.000</option>
                          <option value="1000000">$ 1.000.000</option>
                          <option value="2000000">$ 2.000.000</option>
                          <option value="3000000">$ 3.000.000</option>
                          <option value="4000000">$ 4.000.000</option>
                          <option value="5000000">$ 5.000.000</option>
                          <option value="6000000">$ 6.000.000</option>
                          <option value="7000000" >Mas de $ 6.000.000</option>

                        </select>

	  					<select style="background:#D40E1E;font: 14px Arial, Helvetica, sans-serif;color:#FFFFFF; outline: medium none;width:150px;height:25px;float:left;" id="presupuesto2" name="presupuesto2">

                          <option value="" selected="selected">Todos</option>
                          <option value="100000000">100 millones</option>
                          <option value="200000000">200 millones</option>
                          <option value="300000000">300 millones</option>
                          <option value="400000000">400 millones</option>
                          <option value="500000000">500 millones</option>
                          <option value="600000000" selected="selected">mas de 500 millones</option>

                        </select>
    </td>
  </tr>
  <tr>
    <td>Código</td>
    <td>
    <input style="background:#D40E1E; font: 14px Arial, Helvetica, sans-serif;color:#FFF; outline: medium none;width:148px;height:20px;float:left; ::-webkit-input-placeholder {color:#FFF;}" placeholder=" Código" name="codigo">
    </td>
  </tr>
</table>
<br>
							<a class="button2" type="submit" href="javascript:document.action1.submit();">
                    			Buscar
                   			 </a> &nbsp;&nbsp;&nbsp;
                            <a class="button2" type="submit" href="http://millanyasociados.co/mapa.php">
                    			Buscar en Mapa
                   			 </a>
                           <!--  <a href="javascript:{}" onclick="document.getElementById('action1').submit(); return false;">submit</a>-->
            </form>
				  </div>
                  <br>Sólo para Bogotá<br>
                  <a target="_blank" href="https://www.banco.colpatria.com.co/PagosElectronicos/Referencias.aspx?IdConvenio=2380" class="link"> <img height="40" src="images/colpatria.png" alt=""> </a>
                  <a target="_blank" href="http://www.ellibertador.com.co/" class="link"> <img height="40" src="images/aseguradora.png" alt=""> </a>
					<!--<div class="box-img img1"><img src="images/1page_img1.jpg" alt=""></div>
					<h4 class="padtop">Requisitos y documentos</h4>
					<p class="padbot">Consulte aqui los la documentación necesaria para realizar la consignación de su inmueble con Millan y Asociados, así como las indicaciones para arrendar o comprar un inmueble...</p>-->

				</div>
				<div class="grid_8 padtop3">
					<div class="img1 slideshow">


                   		<img src="images\slide\1.png" width="300" height="280" />
						<img src="images\slide\2.png" width="300" height="280" />
						<img src="images\slide\3.jpg" width="300" height="280" />
						<img src="images\slide\4.jpg" width="300" height="280" />


                    </div>
					<!--<h4 class="padtop">Servicios</h4>
					<p class="padbot">Millan y Asociados les ofrece múltiples servicios de acuerdo a sus necesidades relacionadas con sus inmuebles, encuentre a continuación el detalle de cada uno de ellos...</p>
					<a href="#" class="button">Ver Más</a>-->
				</div>
				<div class="grid_8 padtop3">
					<div class="box-img img1"><img src="images/1page_img3.jpg" alt=""></div>

                    <h4 class="padbop">Inmuebles en Manizales</h4>
					<p class="padbot">Encuentra la mejor selección de inmuebles en la ciudad de Manizales y el eje cafetero.</p>
					<a href="http://www.millanenlinea.com/web/buscar_inmueble.html" class="button">Ver Más</a>
                    <a href="javascript:popUp('consigne.html')" class="button">CONSIGNE SU INMUEBLE </a>
				</div>
			</div>
		</div>
	</div>
	<div class="container_24">
		<div class="wrapper">
			<div class="grid_16 padtop2">

            </div>
			<div class="grid_8 padtop2">

			</div>
		</div>
	</div>
</section>

<!--Slider-->

<!--<div class="main-slider">
<div class="bg-slider">
	<div class="slider">
		<div class="flexslider">
			<ul class="slides">
				<li><img src="images/slide1.jpg" alt=""><div class=" flex-caption"><span>Profesionales en Propiedad Raíz</span></div></li>
				<li><img src="images/slide2.jpg" alt=""><div class=" flex-caption"><span>El mejor servicio de Administración</span></div></li>
				<li><img src="images/slide3.jpg" alt=""><div class=" flex-caption"><span>15 años en el mercado inmobiliario</span></div></li>
			</ul>
		</div>
	</div>
</div>
</div>
-->


<!-- footer -->
<footer>
	<div class="container_24" style="margin-top:-20px">
		<div>
			<div class="grid_24"><!--<a style="margin-top:-5px;" href="index.html" class="link"><img src="images/logo-footer.png" alt=""></a>-->     <strong>Manizales</strong> Cll 21 No. 21 - 45 | Edificio MILLAN & ASOCIADOS | Tel.: (576) 8808383 | www.millanenlinea.com
            <br>
            <strong>Bogotá</strong> Av Carrera 19 # 95 - 55 Oficina 209 | PBX: (571) 7466693| www.millanyasociados.co
            <br>&copy; 2014 | <a target="_blank" href="http://www.dynamicweb.co">Desarrollado por DynamicWeb</a> | <a target="_blank" href="http://www.domus.la">Domus módulo comercial</a>
            <br><img width="70px" height="42" src="images/icontec2.gif" alt="">
            <img width="42px" height="42" src="images/logo_SelloCaldasExcelente.jpg" alt="">
            <a target="_blank" href="http://www.afydi.com/" class="link"> <img width="50px" height="42" src="images/Afydi_new-logo.png" alt=""> </a>
            <!--<a href="https://www.banco.colpatria.com.co/PagosElectronicos/Referencias.aspx?IdConvenio=2380" class="link"> <img width="86px" height="22" src="images/colpatria.jpg" alt=""> </a>-->

             </div>
		</div>
	</div>
</footer>
</body>
</html>
