<?php 
include ("../isabeldemora/src/conexion.php");
//include ("../domus/src/conexion.php");
	/*Conexión con el servidor*/
	$link=ConectarseServidor();

	/*Conexión con la base de datos*/
	ConectarseBaseDatos($link); 
	
if($_REQUEST[ciudad])
{		
	$ciudad=$_REQUEST[ciudad];
	$wer.=" AND inmuebles.ciudad=$ciudad";
	
	$c2="SELECT latitud AS CLAT, longitud AS CLONG FROM ciudad WHERE codigo=$ciudad";
	$consulta_ciudad=consultas($c2);
	$rec_ciudad=recorrer($consulta_ciudad);
	$clat=$rec_ciudad[CLAT];
	$clong=$rec_ciudad[CLONG];
	//echo "latitud=$clat - longitud=$clong<br>";
} else {$clat="4.673611"; $clong="-74.098084";}
if($_REQUEST[gestion])
{		
	$gestion=$_REQUEST[gestion];
	$wer.=" AND inmuebles.gestion=$gestion";
}
if($_REQUEST[zona])
{		
	$zona=$_REQUEST[zona];
	$wer.=" AND inmuebles.zona=$zona";
}
if($_REQUEST[tipo])
{		
	$tipo=$_REQUEST[tipo];
	$wer.=" AND inmuebles.tipo_inmueble=$tipo";
}
$c1="SELECT 
codigo AS COD,
latitud AS LAT,
longitud AS LNG,
habitaciones AS HABS,
area_construida AS AREA,
barrio AS BARRIO,
inmuebles.tipo_inmueble AS CTIPOINMUEBLE,
tipo_inmuebles.tipo_inmueble AS TIPOINMUEBLE, 
tipo_inmuebles.id_tipo_inmueble AS IDTIPOINM,
inmuebles.gestion AS CGESTION,
gestiones.gestion AS GESTION,
gestiones.id_gestion AS IDGESTION

FROM inmuebles, tipo_inmuebles, gestiones

WHERE  inmuebles.tipo_inmueble = tipo_inmuebles.id_tipo_inmueble 
AND inmuebles.estado=1
AND inmuebles.gestion = gestiones.id_gestion".$wer;
$consulta2=consultas($c1);
//echo $c1;



?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Mapa</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width; initial-scale=1.0">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">

<link rel="stylesheet" type="text/css" href="css/component.css" />

<script src="js/jquery-1.7.1.min.js"></script>
<script src="js/script.js"></script>
<script src="js/forms.js"></script>
<script src="js/superfish.js"></script>
<script src="js/jquery.responsivemenu.js"></script>
<script src="js/FF-cash.js"></script>
<script>
$(function(){
	$('#contact-form').forms({ownerEmail:'#'});
});
</script>
<!--[if lt IE 8]>
   <div style=' clear: both; text-align:center; position: relative;'>
     <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
       <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
  </div>
<![endif]-->
<!--[if lt IE 9]>
	<script src="js/html5.js"></script>
	<link rel="stylesheet" href="css/ie.css"> 
<![endif]-->
<!-- GOOGLE MAPS-->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false">
// Cargamos la API
</script>
<script type="text/javascript">
//ll=3.419094,-76.513338&spn=0.046352,0.084543&z=14
  function iniciar() 
{

   var coordenadas = new google.maps.LatLng(<? echo $clat ?>,<? echo $clong ?>);

  var myOptions = {
    zoom: 12,
    center: coordenadas,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  map = new google.maps.Map(document.getElementById("mapa"), myOptions);
		
	 var posy = new Array();
	 var posx = new Array();
	 var cod = new Array();
	 var barrio = new Array();
	 var gestion = new Array();
	 var tipoinm = new Array();
	 var habs = new Array();
	 var area = new Array();
	 
	 
	<? $cont=0; while($consulta=recorrer($consulta2)){ 
	
	?>
	posy[<? echo $cont; ?>]="<? echo $consulta[LAT]; ?>"
	posx[<? echo $cont; ?>]="<? echo $consulta[LNG]; ?>"
    cod[<? echo $cont; ?>]="<? echo $consulta[COD]; ?>"
	barrio[<? echo $cont; ?>]="<? echo $consulta[BARRIO]; ?>"
	gestion[<? echo $cont; ?>]="<? echo $consulta[GESTION]; ?>"
	tipoinm[<? echo $cont; ?>]="<? echo $consulta[TIPOINMUEBLE]; ?>"
	habs[<? echo $cont; ?>]="<? echo $consulta[HABS]; ?>"
	area[<? echo $cont; ?>]="<? echo $consulta[AREA]; ?>"
	
	
	
//alert("POSY="+posy[<? echo $cont; ?>]+" POSX="+posx[<? echo $cont; ?>]);
  <?  $cont++; } ?>
  //marcar punto inicial
  for(var i=0; i<=<? echo $cont; ?>;i++)
  {
	  //alert("vuelta");
	var coordenadas2 = new google.maps.LatLng(posy[i],posx[i]);
	var codigo = cod[i];
	var abarrio = barrio[i];
	var agestion = gestion[i];
	var atipoinm = tipoinm[i];
	var ahabs = habs[i];
	var aarea = area[i];
	
	placeMarker(coordenadas2,codigo,abarrio,agestion,atipoinm,ahabs,aarea);
  }
}

var image = 'img/casita.png';
  //funcion marcar punto
function placeMarker(posicion,codigo,barrio,gestion,tipoinm,habs,area) {
  var marker = new google.maps.Marker({
	  //creamos una marca
      position: posicion, 
	  icon:image,
      map: map
  });
  		google.maps.event.addListener(marker, 'click', function() {
 		infowindow.open(map,marker);
		});
	
	var contentString = '<table width="265" border="0" cellspacing="0" cellpadding="0">'+
         '<tr>'+
           '<td width="10" rowspan="3">&nbsp;</td>'+
           '<td width="1" rowspan="3"></td>'+
           '<td class="blanco centro" bgcolor="#dddddd">Codigo='+codigo+'<br>'+barrio+'</td>'+
         '</tr>'+
         '<tr>'+
           '<td width="254" class="negro 12 centro">'+gestion+'<br>'+tipoinm+'<br>Habitaciones='+habs+'<br>Area='+area+'M&sup2;</td>'+
         '</tr>'+
         '<tr>'+
           '<td class="negro"><a href=\'detalleinmueble.php?numInm='+codigo+'\' target=\'_blank \'>Ver detalles</td>'+
         '</tr>'+
       '</table>';

var infowindow = new google.maps.InfoWindow({
    content: contentString
});
}
</script>
<!--FIN GOOGLE MAPS-->
</head>
<body onLoad="iniciar()">
<!-- header -->
<header>
	<div class="container_24">
		<div class="grid_24">
			<h1 class="fleft"><a href="index.php">millan y asociados</a></h1>
			<ul class="sf-menu">
				<li><a href="index.php">Inicio</a></li>
				<li><a href="nosotros.html">Nosotros</a></li>
                <li><a href="servicios.html">Servicios</a>
                <li class="current"><a href="inmuebles.php">Inmuebles</a></li>
                <li><a>Clientes</a>
                <ul>
                <li><a href="#">Propietarios</a></li>
                <li><a href="#">Arrendatarios</a></li>
                </ul>
                </li>
                <!--<li><a href="index-3.html">Avaluos</a>
                <ul>
                <li><a href="#">Propietarios</a></li>
                <li><a href="#">Arrendatarios</a></li>
                </ul>
                </li>-->
				<li><a href="contacto.php">Contacto</a></li>
			</ul>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</header>

<!-- content -->
<section>
	<div class="bg">
		<div class="container_24">
			<div class="wrapper">
<span style="font:15px/22px Arial, Helvetica, sans-serif;color:#7c7876; float:left">Somos una empresa dedicada a la prestación de servicios inmobiliarios, arrendar, vender y administrar inmuebles..</span>
            <div class="social">
            	<a href="#"><img src="images/pinterest.jpg" height="30" width="30"></a>&nbsp;
            	<a href="#"><img src="images/facebook.png" height="30" width="30"></a>&nbsp;
            	<a href="#"><img src="images/twitter.png" height="30" width="30"></a>&nbsp;
            	<a href="#"><img src="images/youtube.png" height="30" width="30"></a>&nbsp;
            </div>
            
            <div class="container_form">
            <div class="main">
				<form action="inmuebles.php" method="get" name="action1" id="action1" class="cbp-mc-form">
					<div class="cbp-mc-column">
						<label for="ciudad">Ciudad</label>
	  					<select id="ciudad" name="ciudad">
                        <option  selected="selected" value="0">Ciudad [Todas]</option>
        				<?php $instruccion="SELECT DISTINCT ciudad.codigo AS CID, ciudad.nombre AS CNAME 
							FROM ciudad, inmuebles
							WHERE ciudad.codigo=inmuebles.ciudad 
							AND inmuebles.estado=1";
			 				 $respuesta=consultas($instruccion);
			  					while  ( $resrecorrer = recorrer($respuesta) ) 
										{
								$instruccion2="SELECT COUNT(codigo) 
								FROM inmuebles 
								WHERE ciudad=$resrecorrer[CID] AND inmuebles.estado=1";
								$respuesta2=consultas($instruccion2);
								$fila=fila($respuesta2);
								if ($resrecorrer[CID]==$ciudad)
				 {echo "<option selected=\"selected\" value=".$resrecorrer[CID].">".$resrecorrer[CNAME]." ($fila[0])"."</option>"; }
				 else echo "<option value=".$resrecorrer[CID].">".$resrecorrer[CNAME]." ($fila[0])"."</option>"; 
									} 
				
		?>
                        </select>
	  					<label for="gestion">Gestión</label>
	  					<select id="gestion" name="gestion">
                        <option  selected="selected" value="0">Gestion [Todas]</option>
           				 <?php $instruccion="SELECT DISTINCT gestiones.id_gestion AS GID, gestiones.gestion AS GNAME 
							FROM gestiones, inmuebles
							WHERE gestiones.id_gestion=inmuebles.gestion
							AND inmuebles.estado=1";
			 					 $respuesta=consultas($instruccion);
			  						while  ( $resrecorrer = recorrer($respuesta) ) 
									{
								$instruccion2="SELECT COUNT(codigo) FROM inmuebles WHERE gestion=$resrecorrer[GID] AND inmuebles.estado=1";
								$respuesta2=consultas($instruccion2);
								$fila=fila($respuesta2);
								if ($resrecorrer[GID]==$gestion){
					 			echo "<option selected=\"selected\" value=".$resrecorrer[GID].">".$resrecorrer[GNAME]." ($fila[0])"."</option>"; }
								else { echo "<option value=".$resrecorrer[GID].">".$resrecorrer[GNAME]." ($fila[0])"."</option>"; }
						} 
				
		?>
                        </select>
                        <label for="codigo">Código</label>
	  					<input type="codigo" id="codigo" name="codigo" value="  " placeholder="">
	  					
	  				</div>
	  				<div class="cbp-mc-column">
	  					<label for="tipo">Tipo de Inmueble</label>
	  					<select id="tipo" name="tipo">
                        <option  selected="selected" value="0">Tipo Inmueble [Todos]</option>
    						<?php $instruccion="SELECT DISTINCT tipo_inmuebles.id_tipo_inmueble AS TID, tipo_inmuebles.tipo_inmueble AS TNAME 
							FROM tipo_inmuebles, inmuebles
							WHERE tipo_inmuebles.id_tipo_inmueble=inmuebles.tipo_inmueble";
							  $respuesta=consultas($instruccion);
			  				while  ( $resrecorrer = recorrer($respuesta) ) 
								{
									$instruccion2="SELECT COUNT(codigo) FROM inmuebles WHERE tipo_inmueble=$resrecorrer[TID] AND inmuebles.estado=1";
									$respuesta2=consultas($instruccion2);
									$fila=fila($respuesta2);
								if ($resrecorrer[TID]==$tipo)
				 { echo "<option selected=\"selected\" value=".$resrecorrer[TID].">".$resrecorrer[TNAME]." ($fila[0])"."</option>"; }
				 else  { echo "<option value=".$resrecorrer[TID].">".$resrecorrer[TNAME]." ($fila[0])"."</option>"; }
						} 
				
		?>
                        </select>
						<label for="zona">Zona</label>
	  					<select id="zona" name="zona">
                        <option  selected="selected" value="0">Zona [Todas]</option>
						<?php $instruccion="SELECT DISTINCT zonas.id_zona AS ZID, zonas.zona AS ZNAME 
							FROM zonas, inmuebles
							WHERE zonas.id_zona=inmuebles.zona
							AND inmuebles.estado=1";
							  $respuesta=consultas($instruccion);
			  					while  ( $resrecorrer = recorrer($respuesta) ) 
								{
								$instruccion2="SELECT COUNT(codigo) FROM inmuebles WHERE zona=$resrecorrer[ZID] AND inmuebles.estado=1";
								$respuesta2=consultas($instruccion2);
								$fila=fila($respuesta2);
								if ($resrecorrer[ZID]==$zona)
				 {echo "<option selected=\"selected\" value=".$resrecorrer[ZID].">".$resrecorrer[ZNAME]." ($fila[0])"."</option>"; }
				 else {echo "<option value=".$resrecorrer[ZID].">".$resrecorrer[ZNAME]." ($fila[0])"."</option>"; }
						} 
				
		?>
                        </select>
                        <label for="precio">Precio</label>
	  					<select id="precio" name="precio">
                        	<option>100K-350K</option>
        					<option>350K-600K</option>
        					<option>600K-1M</option>
        					<option>1M-2M</option>
                        </select>
                       </div>
                       <div class="cbp-mc-column">
                       <div class="cbp-mc-submit-wrap">
                    		<a href="javascript:document.action1.submit();">
                    			<input class="cbp-mc-submit" type="submit" onclick = "this.form.action = 'inmuebles.php'" value="Buscar Inmuebles" />
                   			 </a>
                    	</div>
                        
                        <div class="cbp-mc-submit-wrap">
                    		<a href="javascript:document.action1.submit();">
                    			<input class="cbp-mc-submit" type="submit" onclick = "this.form.action = 'mapa.php'" value="Buscar en el Mapa" />
                   			 </a>
                    	</div>
                        
                        <div class="cbp-mc-submit-wrap">
                    		<a href="http://www.millanenlinea.com/website/buscar_inmueble.php" target="_blank">
                    			<input class="cbp-mc-submit"  type="button" value="Inmuebles en Manizales" />
                   			 </a>
                    	</div>
                       </div>
               
				</form>
                
                
               </div>
			</div>
		</div>
	</div>
	<div class="container_24">	
</section>

<p>   <div id="mapa"></div></p>

<!-- footer -->
<footer>
	<div class="container_24">
            <div class="cbp-mc-column-foot">
            Bogotá Cra. 12 No. 96 - 81  Oficina 204<br/>
			Edificio Parque 96. PBX: (1) 6910020<br/>
            www.millanyasociadosinmobiliaria.com
            </div>
            
            <div class="cbp-mc-column-foot">
            Manizales Cll 21 No. 21 - 45<br/>
			Edificio MILLAN & ASOCIADOS.<br/>
            Tel.: (6) 8808383   www.millanenlinea.com
            </div>
                       	
    		<div class="cbp-mc-column-foot">
            <img width="50px" height="30" src="images/icontec2.gif" alt=""> <img width="30px" height="30" src="images/logo_SelloCaldasExcelente.jpg" alt=""><br/>
            <a href="http://www.ellibertador.com.co/" class="link"> <img width="150px" height="22" src="images/aseguradora.jpg" alt=""> </a> <a href="https://www.banco.colpatria.com.co/PagosElectronicos/Referencias.aspx?IdConvenio=2380" class="link"> <img width="86px" height="22" src="images/colpatria.jpg" alt=""> </a> <br/>
            &copy; 2013 | <a href="http://domus.net.co/">Domus módulo comercial</a>
            </div>
            
	</div>            
</footer>  

</body>
</html>
