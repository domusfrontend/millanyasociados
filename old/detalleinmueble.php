<?php 
$suc = 100;
$inmo = 10;

//include ("../src/conexion.php");
include ("src/conexion.php");

	/*Conexión con el servidor*/
	$link=ConectarseServidor();

	/*Conexión con la base de datos*/
	ConectarseBaseDatos($link); 
$numgaleria=$_REQUEST[numInm];

$i2="SELECT inmuebles.codigo AS CODIGO,
inmuebles.id AS ID,
video AS VIDEO,
inmuebles.tipo_inmueble AS TIPO,
inmuebles.latitud AS LAT,
inmuebles.longitud AS LNG,
inmuebles.gestion AS CGESTION,
parqueadero AS PARQ,
parq_cubierto AS PARQC,
area_construida AS AREA,
area_lote AS AREA2,
barrio AS BARRIO, 
ciudad.nombre AS CIUDAD,
inmuebles.ciudad AS CODCIUDAD,
canon AS CANON,
venta AS VENTA,
administracion AS ADMON,
habitaciones AS HABS,
banos AS BANOS,
estrato AS ESTRATO,
visitas AS VISITAS,
descripcion AS DESCRIPCION,
tipo_inmuebles.tipo_inmueble AS TIPOINMUEBLE, 
gestiones.gestion AS GESTION,
zonas.zona AS ZONA,
cocina.cocina AS COCINA,
zonaoficios.zonaoficio AS ZOFICIOS
FROM inmuebles, tipo_inmuebles, gestiones, zonas, cocina, zonaoficios, ciudad
WHERE inmuebles.tipo_inmueble = tipo_inmuebles.id_tipo_inmueble
AND inmuebles.gestion = gestiones.id_gestion
AND inmuebles.zona = zonas.id_zona
AND inmuebles.cocina = cocina.id_cocina
AND inmuebles.zona_oficios = zonaoficios.id_zonaoficio
AND inmuebles.ciudad = ciudad.codigo
AND inmuebles.sucursal =".$suc."
AND inmuebles.id=$numgaleria";
$consulta2=consultas($i2);

$fila=recorrer($consulta2);

?>
<!DOCTYPE html>
<html lang="es">
<head>
<title>
Codigo <?php $codigo=$fila["CODIGO"];echo $codigo; ?> , <?php echo $fila["TIPOINMUEBLE"];?> en <?php echo $fila["GESTION"];?> <?php if($fila[TIPO]==1 || $fila[TIPO]==2 || $fila[TIPO]==3 || $fila[TIPO]==10 || $fila[TIPO]==12 || $fila[TIPO]==13 || $fila[TIPO]==14)
		  {echo $fila["HABS"].' Habitaciones';} else echo " "; ?>, <?php echo $fila["AREA"];echo " M&sup2";?>, <?php 
		  $precio=$fila[CANON]+$fila[ADMON];
		  if($fila[CGESTION]==1)
		  { echo "$ ";
		  echo number_format( $precio, 0, ",", ".") ;}
		  
		  elseif($fila[CGESTION]==2)
		   { echo "$ ";
		   echo number_format( $fila[VENTA], 0, ",", ".") ;}
		   
		     elseif($fila[CGESTION]==3)
		   { echo "Venta "."$ " ;
			echo number_format( $fila[VENTA], 0, ",", ".") ;
			echo "Canon "."$ " ;
		   echo number_format( $precio, 0, ",", ".") ;}
		  ?>
</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width; initial-scale=1.0">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">

<link rel="stylesheet" type="text/css" href="css/component.css" />

<script src="js/jquery-1.7.1.min.js"></script>
<script src="js/script.js"></script>
<script src="js/forms.js"></script>
<script src="js/superfish.js"></script>
<script src="js/jquery.responsivemenu.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/FF-cash.js"></script>
<script>
$(function(){
	$('#contact-form').forms({ownerEmail:'#'});
});
</script>
<!--[if lt IE 8]>
   <div style=' clear: both; text-align:center; position: relative;'>
     <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
       <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
  </div>
<![endif]-->
<!--[if lt IE 9]>
	<script src="js/html5.js"></script>
	<link rel="stylesheet" href="css/ie.css"> 
<![endif]-->
<!-- GOOGLE MAPS-->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false">
// Cargamos la API
</script>
<script type="text/javascript">
//ll=3.419094,-76.513338&spn=0.046352,0.084543&z=14
   function iniciar() {
var myLatlng = new google.maps.LatLng(<?php echo $fila['LAT']; ?>,<?php echo $fila['LNG']; ?>);
var myOptions = {
  zoom: 14,
  center: myLatlng,
  mapTypeId: google.maps.MapTypeId.ROADMAP
}

var map = new google.maps.Map(document.getElementById("mapa"), myOptions);

var contentString =  '<table width="265" border="0" cellspacing="0" cellpadding="0">'+
         '<tr>'+
           '<td width="20" rowspan="3">&nbsp;</td>'+
           '<td width="100" rowspan="3"><img src=\'../files/fotos/<?php echo $numgaleria ?>-1.jpg\' border=\'0\' width=\'100\' height=\'100\'></td>'+
           '<td class="ul3" bgcolor="CC0000"><?php echo "Codigo =".$numgaleria."<br>".$fila['BARRIO']; ?></td>'+
         '</tr>'+
         '<tr>'+
           '<td width="100" class="negro"><?php echo $fila['GESTION']."<br>".$fila['TIPOINMUEBLE']."<br> Habitaciones =".$fila['HABS']."<br> Area =".$fila['AREA']."M&sup2"; ?></td>'+
         '</tr>'+
       '</table>';

var infowindow = new google.maps.InfoWindow({
    content: contentString
});
var image = 'images/casita.png';
var marker = new google.maps.Marker({
    position: myLatlng,
    map: map,
    title:'<?php echo "Código = $numgaleria" ;?>',
	icon: image 
});
   
google.maps.event.addListener(marker, 'click', function() {
 
});}
</script>
<!--FIN GOOGLE MAPS-->
<!-- Start VisualSlideShow.com HEAD section -->
	<link rel="stylesheet" type="text/css" href="engine/css/slideshow.css" media="screen" />
	<style type="text/css">.slideshow a#vlb{display:none}</style>
	<script type="text/javascript" src="engine/js/mootools.js"></script>
	<script type="text/javascript" src="engine/js/visualslideshow.js"></script>
	<!-- End VisualSlideShow.com HEAD section -->
</head>
<body onLoad="iniciar()">
<!-- header -->
<header>
	<div class="container_24">
		<div class="grid_24">
			<h1 class="fleft"><a href="index.php">millan y asociados</a></h1>
			<ul class="sf-menu">
				<li><a href="index.php">Inicio</a></li>
				<li><a href="nosotros.html">Nosotros</a></li>
                <li><a href="#">Servicios<span class="arrow"></span></a><ul>
								<li><a href="#">Arrendamiento</a></li>
								<li><a href="#">Compra y Venta de Inmuebles</a></li>
								<li><a href="#">Avalúo de Inmuebles</a></li>
								<li><a href="#">Administración de Inmuebles</a></li>
                                <li><a href="#">Reparaciones Locativas</a></li>
                                <li><a href="#">Seguros</a></li>
							</ul>
						</li>
                <li class="current"><a href="inmuebles.php">Inmuebles</a></li>
                <li><a href="clientes.html">Clientes</a></li>
                <li><a href="contacto.html">Contacto</a></li>
			</ul>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</header>
<!-- content -->
<section>
	<div class="bg">
		<div class="container_24">
			<div class="wrapper">
				<div class="grid_24 padtop33">
					
                    <!--Slider-->
<div class="cbp-mc-column-desc2">
                            <div class="main-slider">
                            <div class="bg-slider">
                                <div class="slider">
                                    <div class="flexslider">
                                        <ul class="slides">
                                            
                                            
                                             <?php
						
		//fotos new
		
 $rutafotos='http://domus.la/comercial/files/fotos/inmobiliaria_'.$inmo.'/';
			$url='http://domus.la/comercial/files/fotos/consulta_fotos.php?codigo='.$codigo.'&inmo='.$inmo ;
			$obj = json_decode(file_get_contents($url),true);
			//echo $obj[0];
			//print_r($obj);
			$numfotos=count($obj);
			 
 for($x=0;$x<$numfotos;$x++)
		{
			$foto = $obj[$x];
			$fotoimp=$rutafotos.$foto;
			if($obj[$x]!='' || $obj[$x]!=0)
			{ 
			$fotos_find++;
			echo "<li><img id=\"slide-".$x."\" src=\"$fotoimp\"  alt=\"\" /></li>";
			}
			if ($fotos_find==0)
			{echo "<img   src=\"images/nodisponible.png\" alt=\"\" />";}
		}
		?>
                                            
<!--        id=\"slide-".$x."\"             <img src="images/slide1.jpg" alt="">-->
                                            
                                            
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            </div>
</div>
  					<!-- Fin Slider-->
                    
                    
                    
		<div class="detalles">
                            
                          <h3 style="text-align:center">  <?php 
		  $precio=$fila[CANON]+$fila[ADMON];
		  if($fila[CGESTION]==1)
		  { echo "$ "."<strong>" ;
		  echo number_format( $precio, 0, ",", ".") ;echo"</strong>";}
		  
		  elseif($fila[CGESTION]==2)
		   { echo "$ "."<strong>" ;
		   echo number_format( $fila[VENTA], 0, ",", ".") ;echo"</strong>";}
		   
		     elseif($fila[CGESTION]==3)
		   { echo "Venta <strong>"."$ " ;
			echo number_format( $fila[VENTA], 0, ",", ".") ;echo"</strong><br>";
			echo "Canon <strong> "."$ " ;
		   echo number_format( $precio, 0, ",", ".") ;	echo"</strong>";
		   }
		  ?> </h3><br>
                            
                            
          <h6 class="padbot2">Código <?php echo $codigo; ?> , <?php echo $fila["TIPOINMUEBLE"];?> en <?php echo $fila["GESTION"];?><br> <?php if($fila[TIPO]==1 || $fila[TIPO]==2 || $fila[TIPO]==3 || $fila[TIPO]==10 || $fila[TIPO]==12 || $fila[TIPO]==13 || $fila[TIPO]==14)
		{echo $fila["HABS"].' Habitaciones';} else echo " "; ?>, <?php echo $fila["AREA"];echo " M&sup2";?></h6>
          <h6 class="padbot2"><strong><?php echo $fila[CIUDAD];?></strong> -  <strong><?php echo $fila[ZONA];?></strong><br><strong><?php echo $fila[BARRIO];?></strong></h6>
          
          
          
          
          
          <h6>Datos generales:</h6>
          
	Tipo de Inmueble: <strong><?php echo $fila["TIPOINMUEBLE"];?></strong><br/>
    Administración: <?php echo "<strong>"."$ " ; 
		  				if($fila[CGESTION]==1){
							if($fila[ADMON]!=0){echo "INCLUIDA";}else{echo "SIN ADMINISTRACION";} echo"</strong>";}
		  				if($fila[CGESTION]==2){ 
				 			if($fila[ADMON]!=0){echo number_format( $fila[ADMON], 0, ",", ".");}else{echo "SIN ADMINISTRACION";} echo"</strong>";}
						if($fila[CGESTION]==3){ 
				 			if($fila[ADMON]!=0){echo number_format( $fila[ADMON], 0, ",", ".");}else{echo "SIN ADMINISTRACION";} echo"</strong>";}
			  		?>
                    
                            <br>
                            
    Área Construída: <strong><?php echo $fila["AREA"];echo " M&sup2";?></strong><br/>
    Habitación(es): <strong><?php if($fila[TIPO]==1 || $fila[TIPO]==2 || $fila[TIPO]==3 || $fila[TIPO]==10 || $fila[TIPO]==12 || $fila							[TIPO]==13 || $fila[TIPO]==14)
		  				{echo $fila["HABS"].' Habitaciones';} else echo "No Aplica"; ?></strong><br/>
	Baños: <strong><?php if($fila[TIPO]==1 || $fila[TIPO]==2 || $fila[TIPO]==3 || $fila[TIPO]==10 || $fila[TIPO]==12 || $fila[TIPO]==13 || $fila[TIPO]==14)
		  				{echo $fila["BANOS"].' Baños'; } else echo "No Aplica"; ?></strong><br>
	Parqueos Totales: <strong><?php echo $fila["PARQ"] ?></strong><br>
    Parqueos Cubiertos: <strong><?php if($fila["PARQC"]=='on'){echo 'Si';}
		   							else if($fila["PARQC"]==''){echo 'No';}
		   							else if($fila["PARQC"]=='off'){echo 'No';}
		   							else {echo $fila["PARQC"];} ?></strong><br>
	Estrato: <strong><?php echo $fila["ESTRATO"];?></strong>
          
          <br/><br/><br/>
          
          <div><span style="color:#00528C">
          <?php 

		  $visitas=$fila["VISITAS"]; 
		  echo "Inmueble visitado $visitas veces";

		  $visitas=$visitas+1;

		  $sql="UPDATE inmuebles SET visitas='".$visitas."' WHERE codigo='$codigo'";

		 $res=consultas($sql);

		   ?></span></div>
           
           
     <!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style ">
<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
<a style="width:100px" class="addthis_button_tweet"></a>
<a class="addthis_counter addthis_pill_style"></a>
</div>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4dcde18b394a19ad"></script>
<!-- AddThis Button END -->
           
           
           
		</div>
        
        		</div>
             </div>
           <div style="text-align:justify;"><h5 class="choose">Descripción</h5><br>
                <p style="text-align:justify; padding:10px 10px 10px 10px; font-size:14px; color:#000;background-color:#FFF"><?php echo $fila[DESCRIPCION];?></p></div>
                
                
               <br>

						
                            <div class="map" id="mapa"></div>
                        
                        <br><br>
                        
			<p>Caracteristicas del inmueble</p>
			<strong>Interiores</strong><br>

<div style="text-align:justify">     
<?php
$sql1='SELECT 
caracteristicas.nombre AS NOMBRE
FROM 
	caracteristicas, inmueble_caracteristicas
WHERE 
	inmueble_caracteristicas.id_inmueble = '.$fila["ID"].'
AND
	inmueble_caracteristicas.id_caracteristica = caracteristicas.id_caracteristica
	AND
	caracteristicas.tipo = 1';
$consulta1=consultas($sql1);

while($fila1=recorrer($consulta1))
{echo $fila1[NOMBRE].', &nbsp;';}

?>
</div>
<br><br>

						<strong>Exteriores</strong><br>
<div style="text-align:justify">
<?php
$sql2='SELECT 
caracteristicas.nombre AS NOMBRE
FROM 
	caracteristicas, inmueble_caracteristicas
WHERE 
	inmueble_caracteristicas.id_inmueble = '.$fila["ID"].'
AND
	inmueble_caracteristicas.id_caracteristica = caracteristicas.id_caracteristica
	AND
	caracteristicas.tipo = 2';
$consulta2=consultas($sql2);

while($fila2=recorrer($consulta2))
{echo $fila2[NOMBRE].', &nbsp;';}

?>
</div>
<br><br>
						<strong>Del Sector</strong><br>

<div style="text-align:justify">
<?php
$sql3='SELECT 
caracteristicas.nombre AS NOMBRE
FROM 
	caracteristicas, inmueble_caracteristicas
WHERE 
	inmueble_caracteristicas.id_inmueble = '.$fila["ID"].'
AND
	inmueble_caracteristicas.id_caracteristica = caracteristicas.id_caracteristica
	AND
	caracteristicas.tipo = 3';
$consulta3=consultas($sql3);

while($fila3=recorrer($consulta3))
{echo $fila3[NOMBRE].', &nbsp;';}

?>
</div>
        
		   
                <div ><br><br>

  
    <?php
	 /*if($fila[VIDEO]!=''){
      echo '<a href="javascript:popUp(\'video.php?numInm='.$numgaleria.'\')">';
      echo '<img src="images/video.png" width="150" height="50" border="0">';
      echo '</a>';
	  }*/
	  ?>
             </div>
				
<div class="wrapper">
				<div class="grid_24 padtop33">
					<h5>¿Interesado en éste inmueble?, Contáctenos:</h5><br>
					<form id="contact-form" action="gcontacto.php">
						<div class="success"> Formulario Enviado. <strong>Estaremos en contacto pronto.</strong></div>
						<fieldset>
							<div class="wrapper">
								<div class="grid_8 suffix_1 alpha">
									<label class="name">
										<input name="nombre" type="text" value="Nombre">
										<span class="error">*No es un nombre válido.</span>
										<span class="empty">*Necesitamos su nombre.</span>
										<span class="clear"></span>
									</label>
									<label class="email">
										<input name="email" type="text" value="E.mail">
										<span class="error">*No es un E.Mail válido.</span>
										<span class="empty">*Necesitamos su E.Mail.</span>
										<span class="clear"></span>
									</label>
									<label class="phone">
										<input name="telefono" type="text" value="Teléfono">
										<span class="error">*No es un teléfono válido.</span>
										<span class="empty">*Necesitamos un teléfono de contacto.</span>
										<span class="clear"></span>
									</label>
								</div>
								<div class="grid_13 suffix_2 omega">
									<label class="message">
										<textarea name="comentario">Mensaje</textarea>
										<span class="error">*Su mensaje es muy corto.</span>
										<span class="empty">*Escríbanos un mensaje por favor.</span>
										<span class="clear"></span>
									</label>
									<div class="buttons"><span><a class="button" data-type="reset">Limpiar</a></span><span><a class="button" data-type="submit" href="javascript:document.action1.submit()">Enviar</a></span></div>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
                
		</div>
	</div>
	
</section>

<!-- footer -->
<footer style="padding-top:20px;">
	<div class="container_24" style="margin-top:20px">
		<div>
			<div class="grid_24"><!--<a style="margin-top:-5px;" href="index.html" class="link"><img src="images/logo-footer.png" alt=""></a>-->     Bogotá Cra. 12 No. 96 - 81  Oficina 204 Edificio Parque 96 | PBX: (571) 6910020 | www.millanyasociadosinmobiliaria.com
            <br>Manizales Cll 21 No. 21 - 45 | Edificio MILLAN & ASOCIADOS | Tel.: (576) 8808383 | www.millanenlinea.com
            <br>&copy; 2014 | <a target="_blank" href="http://www.dynamicweb.co">Desarrollado por DynamicWeb</a> | <a target="_blank" href="http://www.domus.la">Domus módulo comercial</a>
            <br><img width="50px" height="30" src="images/icontec2.gif" alt=""> <img width="30px" height="30" src="images/logo_SelloCaldasExcelente.jpg" alt="">
            <a href="http://www.ellibertador.com.co/" class="link"> <img width="150px" height="22" src="images/aseguradora.jpg" alt=""> </a> <a href="https://www.banco.colpatria.com.co/PagosElectronicos/Referencias.aspx?IdConvenio=2380" class="link"> <img width="86px" height="22" src="images/colpatria.jpg" alt=""> </a>  
            
             </div>
		</div>
	</div>
    <br>
</footer>  
</body>
</html>
