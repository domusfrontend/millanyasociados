<?php
$suc = 100;
$inmo = 10;
//include ("../src/conexion.php");
include ("src/conexion.php");
	/*Conexión con el servidor*/
	$link=ConectarseServidor();

	/*Conexión con la base de datos*/
	ConectarseBaseDatos($link);
	

$i="SELECT codigo AS COD, tipo_inmuebles.tipo_inmueble AS TIPOINMUEBLE, gestiones.gestion AS GESTION, barrio AS BARRIO
FROM inmuebles, tipo_inmuebles, gestiones
WHERE inmuebles.tipo_inmueble = tipo_inmuebles.id_tipo_inmueble
AND inmuebles.gestion = gestiones.id_gestion
AND inmuebles.estado=1
AND inmuebles.destacado = 'ON'";
$queryslider = consultas($i);
?>

<!DOCTYPE html>
<html lang="es">
<head>
<title>millan & asociados</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width; initial-scale=1.0">
<meta name="description" content="Nuestra experiencia de más de 15 años en el mercado nos permite asesorarlo en la compra, venta, 						arrendamiento o avalúo de sus inmuebles." />
<meta name="keywords" content="inmuebles, apartamentos, casas, locales, bodegas, oficinas, fincas, lotes, arrendamiento, arriendo, 					ventas, compra, avaluos, bogota, manizales" />
<meta name="author" content="dynamicweb" />
<link rel="shortcut icon" href="images/simbolo.png">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<script src="js/jquery-1.7.1.min.js"></script>
<script src="js/script.js"></script>
<script src="js/superfish.js"></script>
<script src="js/jquery.responsivemenu.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/FF-cash.js"></script>

    <!--ocultar mostrar-->
<script type="text/javascript" src="js/ocultar.js"></script>
<script type="text/javascript">
$(function(){ ocultarinicio() });
</script>

<style type="text/css">
.slideshow {margin:0 -40px }
.slideshow img { padding: 10px; border: 1px solid #ccc; background-color: #eee; }
</style>
<!-- include jQuery library -->
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>-->
<!-- include Cycle plugin -->
<script type="text/javascript" src="http://malsup.github.com/jquery.cycle.all.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.slideshow').cycle({
		fx: 'fade' // choose your transition type, ex: fade, scrollUp, shuffle, etc...
	});
});
</script>

<!--[if lt IE 8]>
   <div style=' clear: both; text-align:center; position: relative;'>
     <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
       <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
  </div>
<![endif]-->
<!--[if lt IE 9]>
	<script src="js/html5.js"></script>
	<link rel="stylesheet" href="css/ie.css"> 
<![endif]-->
</head>
<body>
<!-- header -->
<header>
	<div class="container_24">
		<div class="grid_24">
			<h1 class="fleft"><a href="index.php">millan y asociados</a></h1>
			<ul class="sf-menu">
				<li class="current"><a href="index.php">Inicio</a></li>
				<li><a href="nosotros.html">Nosotros</a></li>
                <li><a href="servicios.html">Servicios</a><!--<ul>
								<li><a href="#">Arrendamiento<span class="arrow"></span></a></li>
								<li><a href="#">Compra y Venta de Inmuebles</a></li>
								<li><a href="#">Avalúo de Inmuebles</a></li>
								<li><a href="#">Administración de Inmuebles</a></li>
                                <li><a href="#">Asesoría Jurídica</a></li>
                                <li><a href="#">Reparaciones Locativas</a></li>
                                <li><a href="#">Seguros</a></li>
							</ul>-->
						</li>
                <li><a href="inmuebles.php">Inmuebles</a></li>
                <li><a href="clientes.html">Clientes</a></li>
                <li><a href="contacto.html">Contacto</a></li>
			</ul>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</header>

<!-- content -->
<section>
	<div class="bg">
		<div class="container_24">
			<div class="wrapper">
<span style="font:14px/16px Arial, Helvetica, sans-serif;color:#7c7876; float:left; margin-top:8px;">Somos una empresa dedicada a la prestación de servicios inmobiliarios profesionales en las ciudades de Manizales y Bogotá.</span>
            
            <div class="social">
            	<a target="_blank" href="http://instagram.com/millanasociadosinmobiliaria"><img src="images/instagram.png" height="30" width="30"></a>
            	<a target="_blank" href="https://www.facebook.com/MILLANYASOCIADOSINMOBILIARIABOGOTA"><img src="images/facebook.png" height="30" width="30"></a>
            	<a target="_blank" href="https://twitter.com/MILLANASOCIADOS"><img src="images/twitter.png" height="30" width="30"></a>
            	<a target="_blank" href="https://www.linkedin.com/company/3211027?trk=tyah&trkInfo=tarId%3A1407347207177%2Ctas%3Amillan%20%26%20asociados%2Cidx%3A1-1-1"><img src="images/linkedin.png" height="30" width="30"></a>
            </div>
            
				<div class="grid_8 padtop3">
                
                <div class="buscador">
            		
                    
   
 
            
        <form method="get" name="action1" id="action1" action="inmuebles.php">
            <table width="291" height="150" border="0" cellpadding="0" cellspacing="3">
            <tr>
            	<td colspan="2">
            	  Arriendo <input id="gestion" name="gestion" type="radio" value="1" onChange="enableArriendo(1)" checked  >
            	  Venta <input id="gestion" name="gestion" type="radio" value="2" onChange="enableArriendo(2)" >
          	  </td>
                </tr>
    	
  <tr>
  <td height="25">Ciudad</td>
  <td align="right">
    <select style="background:#D40E1E;font: 14px Arial, Helvetica, sans-serif;color:#FFFFFF; outline: medium none;width:150px;height:25px;float:left;" id="ciudad" name="ciudad">
          	<option  selected="selected" value="0">Ciudad [Todas]</option>
           <?php $instruccion="SELECT DISTINCT ciudad.codigo AS CID, ciudad.nombre AS CNAME 
		FROM ciudad, inmuebles
		WHERE ciudad.codigo=inmuebles.ciudad AND inmuebles.estado=1 AND inmuebles.sucursal=".$suc;
			  $respuesta=consultas($instruccion);
			  	while  ( $resrecorrer = recorrer($respuesta) ) 
						{
				$instruccion2="SELECT COUNT(codigo) FROM inmuebles WHERE ciudad=$resrecorrer[CID] AND inmuebles.estado=1 AND inmuebles.sucursal=".$suc;
				$respuesta2=consultas($instruccion2);
				$fila=fila($respuesta2);
				 if ($resrecorrer[CID]==$ciudad){echo "<option  selected=\"selected\" value=".$resrecorrer[CID].">".$resrecorrer[CNAME]." ($fila[0])"."</option>' ";}
				 else
				echo "<option value=".$resrecorrer[CID].">".$resrecorrer[CNAME]." ($fila[0])"."</option>"; 
						} 
				
		?>
        </select>
                
    </td>
  </tr>
  <tr>
    <td>Tipo de Inmueble</td>
    <td align="right">
    <select style="background:#D40E1E;font: 14px Arial, Helvetica, sans-serif;color:#FFFFFF; outline: medium none;width:150px;height:25px;float:left;" id="tipo" name="tipo">
          	<option  selected="selected" value="0">Tipo Inmueble [Todos]</option>
    			<?php $instruccion="SELECT DISTINCT tipo_inmuebles.id_tipo_inmueble AS TID, tipo_inmuebles.tipo_inmueble AS TNAME 
					FROM tipo_inmuebles, inmuebles
					WHERE tipo_inmuebles.id_tipo_inmueble=inmuebles.tipo_inmueble AND inmuebles.sucursal=".$suc;
					  $respuesta=consultas($instruccion);
					while  ( $resrecorrer = recorrer($respuesta) ) 
								{
		$instruccion2="SELECT COUNT(codigo) FROM inmuebles WHERE tipo_inmueble=$resrecorrer[TID] AND inmuebles.estado=1 AND inmuebles.sucursal=".$suc;
							$respuesta2=consultas($instruccion2);
							$fila=fila($respuesta2);
							echo "<option value=".$resrecorrer[TID].">".$resrecorrer[TNAME]."</option>"; 
								} 
				?>
                </select>
    </td>
  </tr>
  <tr>
    <td>Zona</td>
    <td align="right">
    <select style="background:#D40E1E;font: 14px Arial, Helvetica, sans-serif;color:#FFFFFF; outline: medium none;width:150px;height:25px;float:left;" id="zona" name="zona">
                        <option  selected="selected" value="0">Zona [Todas]</option>
						<?php $instruccion="SELECT DISTINCT zonas.id_zona AS ZID, zonas.zona AS ZNAME 
							FROM zonas, inmuebles
							WHERE zonas.id_zona=inmuebles.zona
							AND inmuebles.estado=1 AND inmuebles.sucursal=".$suc;
							  $respuesta=consultas($instruccion);
			  					while  ( $resrecorrer = recorrer($respuesta) ) 
								{
								$instruccion2="SELECT COUNT(codigo) FROM inmuebles WHERE zona=$resrecorrer[ZID] AND inmuebles.estado=1  AND inmuebles.sucursal=".$suc;
								$respuesta2=consultas($instruccion2);
								$fila=fila($respuesta2);
								echo "<option value=".$resrecorrer[ZID].">".$resrecorrer[ZNAME]."</option>"; 
								} 
				
		?>
                        </select>
   
    </td>
  </tr>
  <tr>
    <td>Precio Máximo</td>
    <td align="right">
    <select style="background:#D40E1E;font: 14px Arial, Helvetica, sans-serif;color:#FFFFFF; outline: medium none;width:150px;height:25px;float:left;" id="presupuesto" name="presupuesto">
                        
                          <option value="500000">$ 500.000</option>
                          <option value="1000000">$ 1.000.000</option>
                          <option value="2000000">$ 2.000.000</option>
                          <option value="3000000">$ 3.000.000</option>
                          <option value="4000000">$ 4.000.000</option>
                          <option value="5000000">$ 5.000.000</option>
                          <option value="6000000">$ 6.000.000</option>
                          <option value="7000000" selected="selected">$ 7.000.000 o Más</option>
                        
                        </select>
                        
	  					<select style="background:#D40E1E;font: 14px Arial, Helvetica, sans-serif;color:#FFFFFF; outline: medium none;width:150px;height:25px;float:left;" id="presupuesto2" name="presupuesto2">
                        
                          <option value="100000000">100 millones</option>
                          <option value="200000000">200 millones</option>
                          <option value="300000000">300 millones</option>
                          <option value="400000000">400 millones</option>
                          <option value="500000000">500 millones</option>
                          <option value="600000000" selected="selected">600 millones o Más</option>
                        
                        </select>
    </td>
  </tr>
  <tr>
    <td></td>
  </tr>
</table>
							<a class="button2" type="submit" href="javascript:document.action1.submit();">
                    			Buscar
                   			 </a> &nbsp;&nbsp;&nbsp;
                            <a class="button2" type="submit" href="http://dynamicweb.co/millan/web/mapa.php">
                    			Buscar en Mapa
                   			 </a>
                           <!--  <a href="javascript:{}" onclick="document.getElementById('action1').submit(); return false;">submit</a>-->
            </form>
				  </div>
                  <br><br>Sólo para Bogotá<br>
                  <a target="_blank" href="https://www.banco.colpatria.com.co/PagosElectronicos/Referencias.aspx?IdConvenio=2380" class="link"> <img height="40" src="images/colpatria.png" alt=""> </a>
                  <a target="_blank" href="http://www.ellibertador.com.co/" class="link"> <img height="40" src="images/aseguradora.png" alt=""> </a>
					<!--<div class="box-img img1"><img src="images/1page_img1.jpg" alt=""></div>
					<h4 class="padtop">Requisitos y documentos</h4>
					<p class="padbot">Consulte aqui los la documentación necesaria para realizar la consignación de su inmueble con Millan y Asociados, así como las indicaciones para arrendar o comprar un inmueble...</p>-->
					
				</div>
				<div class="grid_8 padtop3">
					<div class="img1 slideshow">
                                         
                    
                   		<img src="images\slide\1.png" width="300" height="280" />
						<img src="images\slide\2.png" width="300" height="280" />
						<img src="images\slide\3.jpg" width="300" height="280" />
						
                        
                        
                    </div>
					<!--<h4 class="padtop">Servicios</h4>
					<p class="padbot">Millan y Asociados les ofrece múltiples servicios de acuerdo a sus necesidades relacionadas con sus inmuebles, encuentre a continuación el detalle de cada uno de ellos...</p>
					<a href="#" class="button">Ver Más</a>-->
				</div>
				<div class="grid_8 padtop3">
					<div class="box-img img1"><img src="images/1page_img3.jpg" alt=""></div>
					
                    <h4 class="padbop">Inmuebles en Manizales</h4>
					<p class="padbot">Encuentra la mejor selección de inmuebles en la ciudad de Manizales y el eje cafetero.</p>
					<a href="http://www.millanenlinea.com/web/buscar_inmueble.html" class="button">Ver Más</a> <a href="#" class="button">CONSIGNE SU INMUEBLE </a>
				</div>
			</div>
		</div>
	</div>
	<div class="container_24">
		<div class="wrapper">
			<div class="grid_16 padtop2">
			
            </div>
			<div class="grid_8 padtop2">
				
			</div>
		</div>
	</div>
</section>

<!--Slider-->

<!--<div class="main-slider">
<div class="bg-slider">
	<div class="slider">
		<div class="flexslider">
			<ul class="slides">
				<li><img src="images/slide1.jpg" alt=""><div class=" flex-caption"><span>Profesionales en Propiedad Raíz</span></div></li>
				<li><img src="images/slide2.jpg" alt=""><div class=" flex-caption"><span>El mejor servicio de Administración</span></div></li>
				<li><img src="images/slide3.jpg" alt=""><div class=" flex-caption"><span>15 años en el mercado inmobiliario</span></div></li>
			</ul>
		</div>
	</div>
</div>
</div>
-->


<!-- footer -->
<footer>
	<div class="container_24" style="margin-top:-20px">
		<div>
			<div class="grid_24"><!--<a style="margin-top:-5px;" href="index.html" class="link"><img src="images/logo-footer.png" alt=""></a>-->     <strong>Manizales</strong> Cll 21 No. 21 - 45 | Edificio MILLAN & ASOCIADOS | Tel.: (576) 8808383 | www.millanenlinea.com
            <br><strong>Bogotá</strong> Cra. 12 No. 96 - 81  Oficina 204 Edificio Parque 96 | PBX: (571) 6910020 | www.millanyasociadosinmobiliaria.com
            <br>&copy; 2014 | <a target="_blank" href="http://www.dynamicweb.co">Desarrollado por DynamicWeb</a> | <a target="_blank" href="http://www.domus.la">Domus módulo comercial</a>
            <br><img width="70px" height="42" src="images/icontec2.gif" alt="">
            <img width="42px" height="42" src="images/logo_SelloCaldasExcelente.jpg" alt="">
            <a target="_blank" href="http://www.afydi.com.co/" class="link"> <img width="50px" height="42" src="images/Afydi_new-logo.png" alt=""> </a>
            <!--<a href="https://www.banco.colpatria.com.co/PagosElectronicos/Referencias.aspx?IdConvenio=2380" class="link"> <img width="86px" height="22" src="images/colpatria.jpg" alt=""> </a>-->
            
             </div>
		</div>
	</div>
</footer> 
</body>
</html>
